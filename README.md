#Overview
	This is an analysis of Kickstarter data to help create a strategy for the play Fever. These in particular, this analysis uses the date the campaign started and the goal amount. This also includes line graphs in to help with visualizations

##Analysis & Challenges
	I had some challenges regarding the formulas in the outcomes by goals, mostly minor problems such as forgetting a $ or a = at certain points. 

##Results
1.	It looks like May is the best time to launch theater campaigns and that the beginning of the year is the worst. However failed campaigns stayed relatively steady and so the increased number of successful campaigns could be due to an increase in total campaigns. 
2.	It appears as if the less money you are looking for the more likely the project is to be successful.
3.	We could also make an analysis based off the number of backers to outcomes.  
